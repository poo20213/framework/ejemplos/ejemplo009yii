<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conduce".
 *
 * @property string $camionero
 * @property string $camion
 *
 * @property Camiones $camion0
 * @property Camioneros $camionero0
 */
class Conduce extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conduce';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['camionero', 'camion'], 'required'],
            [['camionero'], 'string', 'max' => 15],
            [['camion'], 'string', 'max' => 20],
            [['camionero', 'camion'], 'unique', 'targetAttribute' => ['camionero', 'camion']],
            [['camion'], 'exist', 'skipOnError' => true, 'targetClass' => Camiones::className(), 'targetAttribute' => ['camion' => 'matricula']],
            [['camionero'], 'exist', 'skipOnError' => true, 'targetClass' => Camioneros::className(), 'targetAttribute' => ['camionero' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'camionero' => 'Camionero',
            'camion' => 'Camion',
        ];
    }

    /**
     * Gets query for [[Camion0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamion0()
    {
        return $this->hasOne(Camiones::className(), ['matricula' => 'camion']);
    }

    /**
     * Gets query for [[Camionero0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamionero0()
    {
        return $this->hasOne(Camioneros::className(), ['dni' => 'camionero']);
    }
}
