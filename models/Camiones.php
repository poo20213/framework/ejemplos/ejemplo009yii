<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camiones".
 *
 * @property string|null $tipo
 * @property string|null $modelo
 * @property string $matricula
 * @property int|null $potencia
 *
 * @property Camioneros[] $camioneros
 * @property Conduce[] $conduces
 */
class Camiones extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camiones';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['matricula'], 'required'],
            [['potencia'], 'integer'],
            [['tipo', 'modelo'], 'string', 'max' => 100],
            [['matricula'], 'string', 'max' => 20],
            [['matricula'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tipo' => 'Tipo',
            'modelo' => 'Modelo',
            'matricula' => 'Matricula',
            'potencia' => 'Potencia',
        ];
    }

    /**
     * Gets query for [[Camioneros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamioneros()
    {
        return $this->hasMany(Camioneros::className(), ['dni' => 'camionero'])->viaTable('conduce', ['camion' => 'matricula']);
    }

    /**
     * Gets query for [[Conduces]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConduces()
    {
        return $this->hasMany(Conduce::className(), ['camion' => 'matricula']);
    }
}
