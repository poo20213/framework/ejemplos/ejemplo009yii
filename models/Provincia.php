<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "provincia".
 *
 * @property string|null $nombre
 * @property int $codigo
 *
 * @property Destinatario[] $destinatarios
 * @property Paquetes[] $paquetes
 */
class Provincia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre',
            'codigo' => 'Codigo',
        ];
    }

    /**
     * Gets query for [[Destinatarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDestinatarios()
    {
        return $this->hasMany(Destinatario::className(), ['provincia' => 'codigo']);
    }

    /**
     * Gets query for [[Paquetes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetes()
    {
        return $this->hasMany(Paquetes::className(), ['codigo' => 'paquetes'])->viaTable('destinatario', ['provincia' => 'codigo']);
    }
}
