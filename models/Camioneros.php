<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camioneros".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $poblacion
 * @property string|null $direccion
 * @property int|null $telefono
 * @property int|null $salario
 *
 * @property Camiones[] $camions
 * @property Conduce[] $conduces
 * @property Distribuye[] $distribuyes
 * @property Paquetes[] $paquetes
 */
class Camioneros extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camioneros';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['telefono', 'salario'], 'integer'],
            [['dni'], 'string', 'max' => 15],
            [['nombre', 'poblacion', 'direccion'], 'string', 'max' => 100],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'poblacion' => 'Poblacion',
            'direccion' => 'Direccion',
            'telefono' => 'Telefono',
            'salario' => 'Salario',
        ];
    }

    /**
     * Gets query for [[Camions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamions()
    {
        return $this->hasMany(Camiones::className(), ['matricula' => 'camion'])->viaTable('conduce', ['camionero' => 'dni']);
    }

    /**
     * Gets query for [[Conduces]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConduces()
    {
        return $this->hasMany(Conduce::className(), ['camionero' => 'dni']);
    }

    /**
     * Gets query for [[Distribuyes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDistribuyes()
    {
        return $this->hasMany(Distribuye::className(), ['camionero' => 'dni']);
    }

    /**
     * Gets query for [[Paquetes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPaquetes()
    {
        return $this->hasMany(Paquetes::className(), ['codigo' => 'paquetes'])->viaTable('distribuye', ['camionero' => 'dni']);
    }
}
