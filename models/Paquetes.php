<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "paquetes".
 *
 * @property int $codigo
 * @property string|null $descripcion
 * @property string|null $destinatario
 * @property string|null $direccion_destinatario
 *
 * @property Camioneros[] $camioneros
 * @property Destinatario $destinatario0
 * @property Distribuye $distribuye
 * @property Provincia[] $provincias
 */
class Paquetes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'paquetes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'integer'],
            [['descripcion'], 'string', 'max' => 500],
            [['destinatario', 'direccion_destinatario'], 'string', 'max' => 200],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'descripcion' => 'Descripcion',
            'destinatario' => 'Destinatario',
            'direccion_destinatario' => 'Direccion Destinatario',
        ];
    }

    /**
     * Gets query for [[Camioneros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCamioneros()
    {
        return $this->hasMany(Camioneros::className(), ['dni' => 'camionero'])->viaTable('distribuye', ['paquetes' => 'codigo']);
    }

    /**
     * Gets query for [[Destinatario0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDestinatario0()
    {
        return $this->hasOne(Destinatario::className(), ['paquetes' => 'codigo']);
    }

    /**
     * Gets query for [[Distribuye]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDistribuye()
    {
        return $this->hasOne(Distribuye::className(), ['paquetes' => 'codigo']);
    }

    /**
     * Gets query for [[Provincias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProvincias()
    {
        return $this->hasMany(Provincia::className(), ['codigo' => 'provincia'])->viaTable('destinatario', ['paquetes' => 'codigo']);
    }
}
