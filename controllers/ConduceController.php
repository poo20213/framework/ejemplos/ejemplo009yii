<?php

namespace app\controllers;

use app\models\Conduce;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ConduceController implements the CRUD actions for Conduce model.
 */
class ConduceController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Conduce models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Conduce::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'camionero' => SORT_DESC,
                    'camion' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Conduce model.
     * @param string $camionero Camionero
     * @param string $camion Camion
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($camionero, $camion)
    {
        return $this->render('view', [
            'model' => $this->findModel($camionero, $camion),
        ]);
    }

    /**
     * Creates a new Conduce model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Conduce();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'camionero' => $model->camionero, 'camion' => $model->camion]);
            }
        } else {
            $model->loadDefaultValues();
        }
        
        // creo una variable de tip array con todos loas camioneros
        // select * from camioneros
        $camionero= \app\models\Camioneros::find()->all(); 
        //crear un array donde el indice sea el dni del camionero y el valor el nombre
        $listadoCamioneros= \yii\helpers\ArrayHelper::map($camionero, 'dni', 'nombre');
        
        // creo uan variable de tipo array con todos loa camiones
        // select * from camiones
        $camion= \app\models\Camiones::find()->all(); 
        // crear un array donde el indice sea la matricula y el valor el modelo
        $listadoCamiones= \yii\helpers\ArrayHelper::map($camion, 'matricula', 'modelo');

        return $this->render('create', [
            'model' => $model,
            'listadoCamioneros'=>$listadoCamioneros,
            'listadoCamiones'=> $listadoCamiones,
        ]);
    }

    /**
     * Updates an existing Conduce model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $camionero Camionero
     * @param string $camion Camion
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($camionero, $camion)
    {
        $model = $this->findModel($camionero, $camion);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'camionero' => $model->camionero, 'camion' => $model->camion]);
        }
        $camioneros= \app\models\Camioneros::find()->all(); 
        $listadoCamioneros= \yii\helpers\ArrayHelper::map($camioneros, 'dni', 'nombre');

        $camiones= \app\models\Camiones::find()->all(); 
        $listadoCamiones= \yii\helpers\ArrayHelper::map($camiones, 'matricula', 'modelo');

        return $this->render('update', [
            'model' => $model,
            'listadoCamioneros'=>$listadoCamioneros,
            'listadoCamiones'=> $listadoCamiones,
        ]);
    }

    /**
     * Deletes an existing Conduce model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $camionero Camionero
     * @param string $camion Camion
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($camionero, $camion)
    {
        $this->findModel($camionero, $camion)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Conduce model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $camionero Camionero
     * @param string $camion Camion
     * @return Conduce the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($camionero, $camion)
    {
        if (($model = Conduce::findOne(['camionero' => $camionero, 'camion' => $camion])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
