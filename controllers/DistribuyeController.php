<?php

namespace app\controllers;

use app\models\Distribuye;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DistribuyeController implements the CRUD actions for Distribuye model.
 */
class DistribuyeController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Distribuye models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Distribuye::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'camionero' => SORT_DESC,
                    'paquetes' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Distribuye model.
     * @param string $camionero Camionero
     * @param int $paquetes Paquetes
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($camionero, $paquetes)
    {
        return $this->render('view', [
            'model' => $this->findModel($camionero, $paquetes),
        ]);
    }

    /**
     * Creates a new Distribuye model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Distribuye();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'camionero' => $model->camionero, 'paquetes' => $model->paquetes]);
            }
        } else {
            $model->loadDefaultValues();
        }
        $camioneros= \app\models\Camioneros::find()->all(); // select * from autores
        $listadoCamioneros= \yii\helpers\ArrayHelper::map($camioneros, 'dni', 'nombre');

        $paquete= \app\models\Paquetes::find()->all(); // select * from libros
        $listadoPaquete= \yii\helpers\ArrayHelper::map($paquete, 'codigo', 'descripcion');

        return $this->render('create', [
            'model' => $model,
            'listadoPaquete' => $listadoPaquete,
            'listadoCamioneros' => $listadoCamioneros,
        ]);
    }

    /**
     * Updates an existing Distribuye model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $camionero Camionero
     * @param int $paquetes Paquetes
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($camionero, $paquetes)
    {
        $model = $this->findModel($camionero, $paquetes);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'camionero' => $model->camionero, 'paquetes' => $model->paquetes]);
        }
        $camioneros= \app\models\Camioneros::find()->all(); // select * from autores
        $listadoCamioneros= \yii\helpers\ArrayHelper::map($camioneros, 'dni', 'nombre');
        
        $paquete= \app\models\Paquetes::find()->all(); // select * from libros
        $listadoPaquete= \yii\helpers\ArrayHelper::map($paquete, 'codigo', 'descripcion');

        return $this->render('update', [
            'model' => $model,
            'listadoPaquete' => $listadoPaquete,
            'listadoCamioneros' => $listadoCamioneros
        ]);
    }

    /**
     * Deletes an existing Distribuye model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $camionero Camionero
     * @param int $paquetes Paquetes
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($camionero, $paquetes)
    {
        $this->findModel($camionero, $paquetes)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Distribuye model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $camionero Camionero
     * @param int $paquetes Paquetes
     * @return Distribuye the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($camionero, $paquetes)
    {
        if (($model = Distribuye::findOne(['camionero' => $camionero, 'paquetes' => $paquetes])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
