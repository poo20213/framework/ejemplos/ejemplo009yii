<?php

namespace app\controllers;

use app\models\Destinatario;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DestinatarioController implements the CRUD actions for Destinatario model.
 */
class DestinatarioController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Destinatario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Destinatario::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'provincia' => SORT_DESC,
                    'paquetes' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Destinatario model.
     * @param int $provincia Provincia
     * @param int $paquetes Paquetes
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($provincia, $paquetes)
    {
        return $this->render('view', [
            'model' => $this->findModel($provincia, $paquetes),
        ]);
    }

    /**
     * Creates a new Destinatario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Destinatario();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'provincia' => $model->provincia, 'paquetes' => $model->paquetes]);
            }
        } else {
            $model->loadDefaultValues();
        }
        //$provincia= \app\models\Provincia::find()->all(); 
        //$listadoProvincia= \yii\helpers\ArrayHelper::map($provincia, 'codigo', 'nombre');

       // $paquete= \app\models\Paquetes::find()->all(); 
        //$listadoPaquete= \yii\helpers\ArrayHelper::map($paquete, 'codigo', 'descripcion');

        return $this->render('create', [
            'model' => $model,
           // 'listadoPaquete' => $listadoPaquete,
            //'listadoProvincia' => $listadoProvincia,
        ]);
    }

    /**
     * Updates an existing Destinatario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $provincia Provincia
     * @param int $paquetes Paquetes
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($provincia, $paquetes)
    {
        $model = $this->findModel($provincia, $paquetes);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'provincia' => $model->provincia, 'paquetes' => $model->paquetes]);
        }
        //$provincias= \app\models\Provincia::find()->all(); // select * from autores
        //$listadoProvincia= \yii\helpers\ArrayHelper::map($provincias, 'codigo', 'nombre');

        //$paquete= \app\models\Paquetes::find()->all(); // select * from libros
        //$listadoPaquete= \yii\helpers\ArrayHelper::map($paquete, 'codigo', 'descripcion');

        return $this->render('update', [
            'model' => $model,
           //'listadoPaquete' => $listadoPaquete,
           //'listadoProvincia' => $listadoProvincia,
        ]);
    }

    /**
     * Deletes an existing Destinatario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $provincia Provincia
     * @param int $paquetes Paquetes
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($provincia, $paquetes)
    {
        $this->findModel($provincia, $paquetes)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Destinatario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $provincia Provincia
     * @param int $paquetes Paquetes
     * @return Destinatario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($provincia, $paquetes)
    {
        if (($model = Destinatario::findOne(['provincia' => $provincia, 'paquetes' => $paquetes])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
