<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Camiones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="camiones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Camiones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'tipo',
            'modelo',
            'matricula',
            'potencia',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
