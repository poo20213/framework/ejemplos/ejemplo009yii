<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Destinatario */

$this->title = 'Update Destinatario: ' . $model->provincia;
$this->params['breadcrumbs'][] = ['label' => 'Destinatarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->provincia, 'url' => ['view', 'provincia' => $model->provincia, 'paquetes' => $model->paquetes]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="destinatario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        //'listadoPaquete' => $listadoPaquete,
        //'listadoProvincia' => $listadoProvincia,
    ]) ?>

</div>
