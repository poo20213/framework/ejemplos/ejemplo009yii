<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Destinatario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="destinatario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'provincia')->dropDownList($model->getProvincias()) ?>

    <?= $form->field($model, 'paquetes')->dropDownList($model->getPaquetes()) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
