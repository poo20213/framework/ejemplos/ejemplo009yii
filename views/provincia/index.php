<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Provincias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provincia-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Provincia', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nombre',
            'codigo',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=> '{view} {delete} {update}',
                'buttons'=>[
                    'view' => function ($url, $model){
                        return Html::a('<i class="far fa-eye"></i>',['view','codigo'=>$model->codigo]);
                    },
                    'update' => function ($url, $model){
                        return Html::a('<i class="fas fa-pencil-alt"></i>',['update','codigo'=>$model->codigo]);
                    },
                    'delete' => function ($url, $model){
                        return Html::a('<i class="fas fa-trash-alt"></i>', ['delete', 'codigo' => $model->codigo], [                            
                            'data' => [
                                    'confirm' => '¿Seguro que lo quieres borrar?',
                                    'method' => 'post',
                            ],
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>


</div>
